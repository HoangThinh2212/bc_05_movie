import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import NotFoundPage from "./Pages/NotFoundPage/NotFoundPage";
import DetailPage from "./Pages/DetailPage/DetailPage";
import Layout from "./HOC/Layout";
import UserAdminPage from "./Pages/UserAdminPage/UserAdminPage";
import Spinner from "./Components/Spinner/Spinner";
// imp
function App() {
  return (
    <div>
      <Spinner/>
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <Layout>
                <HomePage />
              </Layout>
            }
          />
          <Route path="/login" element={<LoginPage />} />
          <Route
            path="/detail/:id"
            element={
              <Layout>
                <DetailPage />
              </Layout>
            }
          />

          {/* Admin module */}

          <Route
            path="admin/user"
            element={
              <Layout>
                <UserAdminPage />
              </Layout>
            }
          />

          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
// redux toolkit
