export const USER_LOGIN = "USER_LOGIN";

export const userLocalService = {
  set: (userData) => {
    let userJson = JSON.stringify(userData);
    localStorage.setItem(USER_LOGIN, userJson);
  },
  get: () => {
    let userJSon = localStorage.getItem(USER_LOGIN);
    if (userJSon != null) {
      return JSON.parse(userJSon);
    } else {
      return null;
    }
  },
  remove: () => {
    localStorage.removeItem(USER_LOGIN);
  },
};
