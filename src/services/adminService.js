import { https } from "./configURL";

export const adminService = {
  layDanhSachNguoiDung: () => {
    return https.get("/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP06");
  },
  xoaNguoiDung: (idUser) => {
    return https.delete(`/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${idUser}`);
  },
};
