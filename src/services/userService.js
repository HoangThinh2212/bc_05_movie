import axios from "axios";
import { BASE_URL, createConfig } from "./configURL";
export const userService = {
  postDangNhap: (dataUser) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
      method: "POST",
      data: dataUser,
      headers: createConfig(),
    });
  },
  getDanhSachPhim: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim`,
      method: "GET",
      headers: createConfig(),
    });
  },
};
// npm i axios
