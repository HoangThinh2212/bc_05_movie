import axios from "axios";
import { userLocalService } from "./localStorageService";
export const BASE_URL = "https://movienew.cybersoft.edu.vn";

export const TOKEN_CYBERSOFT =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzNSIsIkhldEhhblN0cmluZyI6IjAzLzA2LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY4NTc1MDQwMDAwMCIsIm5iZiI6MTY1NzczMTYwMCwiZXhwIjoxNjg1ODk4MDAwfQ.KXn1XtehbphvfW3OSUFlLIzSrEtSLDtDQG4BgF38Cus";

export const createConfig = () => {
  return {
    TokenCybersoft: TOKEN_CYBERSOFT,
    Authorization: "bearer " + userLocalService.get()?.accessToken,
  };
};

export const https = axios.create({
  baseURL: BASE_URL,
  headers: createConfig(),
});

// Interceptor axios
// Add a request interceptor
https.interceptors.request.use(function (config) {
  // Do something before request is sent
  console.log("request");
  return config;
}, function (error) {
  // Do something with request error
  return Promise.reject(error);
});

// Add a response interceptor
https.interceptors.response.use(function (response) {
  // Any status code that lie within the range of 2xx cause this function to trigger
  // Do something with response data
  console.log('response: ', response);
  return response;
}, function (error) {
  // Any status codes that falls outside the range of 2xx cause this function to trigger
  // Do something with response error
  return Promise.reject(error);
});
