import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { userService } from "../../services/userService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { SET_USER_INFOR } from "../../redux/constant/userContant";
import { userLocalService } from "../../services/localStorageService";
import Lottie from "lottie-react";
import bg_animate from "../../assets/90315-christmas-tree.json";
import {
  setLoginAction,
  setLoginActionService,
} from "../../redux/actions/userAction";
import { setUserInfor } from "../../redux-toolkit/slice/userSlice";
export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    console.log("Success:", values);
    userService
      .postDangNhap(values)
      .then((res) => {
        dispatch(setUserInfor(res.data.content));
        console.log(res);
        message.success("Đăng nhập thành công");
        //  lưu vào localStore
        userLocalService.set(res.data.content);
        // delay 1s để show thông báo
        setTimeout(() => {
          // windown.location.heft => reload toàn bộ trang
          // window.location.href = "/";
          // dùng navigate để ko bị relod trang
          navigate("/");
        }, 1000);
      })
      .catch((err) => {
        message.error("Đã có lỗi xảy ra");
        console.log(err);
      });
  };

  const onFinishReduxThunk = (value) => {
    let onNavigate = () => {
      setTimeout(() => {
        navigate("/");
      }, 1000);
    };
    dispatch(setLoginActionService(value, onNavigate));
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="w-screen h-screen flex justify-center items-center">
      <div className="container p-5 flex">
        <div className="h-full w-1/2">
          <Lottie animationData={bg_animate} loop={true} />
        </div>
        <div className="h-full w-1/2 pt-20">
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              wrapperCol={{
                span: 24,
              }}
              className="text-center"
            >
              <Button
                className="bg-red-500 text-white hover:bg-white hover:text-red-600"
                htmlType="submit"
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
