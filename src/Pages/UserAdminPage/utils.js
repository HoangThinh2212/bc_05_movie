import { Tag } from "antd";

export const columnsUser = [
  {
    title: "Tài khoản",
    dataIndex: "taiKhoan",
    key: "taiKhoan",
  },
  {
    title: "Họ tên",
    dataIndex: "hoTen",
    key: "hoTen",
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
  },
  {
    title: "Số ĐT",
    dataIndex: "soDT",
    key: "soDT",
  },
  {
    title: "Mật khẩu",
    dataIndex: "matKhau",
    key: "matKhau",
  },
  {
    title: "Loại khách hàng",
    dataIndex: "maLoaiNguoiDung",
    key: "maLoaiNguoiDung",
    render: (text) => {
      if (text === "QuanTri") {
        return <Tag color="red">Quản trị</Tag>;
      } else {
        return <Tag color="blue">Khách hàng</Tag>;
      }
    },
  },
  {
    title: "Thao tác",
    dataIndex: "action",
    key: "action",
  },
];
