import React, { useEffect, useState } from "react";
// import Table from antd
import { adminService } from "../../services/adminService";
import { Table, message } from "antd";
import { columnsUser } from "./utils";
import Spinner from "../../Components/Spinner/Spinner";

export default function UserAdminPage() {
  const [userArr, setUserArr] = useState([]);
  useEffect(() => {
    let handleDeleteUser = (idUser) => {
      adminService
        .xoaNguoiDung(idUser)
        .then((res) => {
          message.success("Xóa người dùng thành công");
          console.log(res);
          fetchUserList();
        })
        .catch((err) => {
          message.error(err.response.data.content);
          console.log(err);
        });
    };

    let fetchUserList = () => {
      adminService
        .layDanhSachNguoiDung()
        .then((res) => {
          console.log(res);
          let userList = res.data.content.map((item) => {
            return {
              ...item,
              action: (
                <>
                  <button
                    className="px-2 py-1 rounded bg-red-500 text-white"
                    onClick={() => {
                      handleDeleteUser(item.taiKhoan);
                    }}
                  >
                    Xóa
                  </button>
                  <button className="px-2 py-1 rounded bg-blue-500 text-white">
                    Sửa
                  </button>
                </>
              ),
            };
          });
          setUserArr(userList);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    fetchUserList();
  }, []);
  return (
    <div className="container mx-auto">
      <Table columns={columnsUser} dataSource={userArr} />;
    </div>
  );
}
