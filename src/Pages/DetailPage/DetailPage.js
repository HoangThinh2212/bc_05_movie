import React from "react";
import { useParams } from "react-router-dom";
import Header from "../../Components/Header/Header";

export default function DetailPage() {
  let params = useParams();
  console.log(`  🚀 __ file: DetailPage.js:6 __ params`, params);
  return <div>{params.id}</div>;
}
