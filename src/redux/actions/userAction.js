import { message } from "antd";
import { userLocalService } from "../../services/localStorageService";
import { userService } from "../../services/userService";
import { SET_USER_INFOR } from "../constant/userContant";

export const setLoginAction = (value) => {
  // api here
  return {
    type: SET_USER_INFOR,
    payload: value,
  };
};

export const setLoginActionService = (userForm, onSuccess) => {
  return (dipatch) => {
    userService
      .postDangNhap(userForm)
      .then((res) => {
        message.success("Đăng nhập thành công");
        // lưu vào localStore
        userLocalService.set(res.data.content);
        console.log(res);
        // đẩy lên redux store
        dipatch({
          type: SET_USER_INFOR,
          payload: res.data.content,
        });
        onSuccess();
      })
      .catch((err) => {
        message.error("Đã có lỗi xảy ra");
        console.log(err);
      });
  };
};
