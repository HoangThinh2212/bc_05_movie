import React from "react";
import { PacmanLoader } from "react-spinners";

export default function Spinner() {
  return (
    <div className="fixed w-screen h-screen bg-black z-50 top-0 left-0 flex items-center justify-center">
      <PacmanLoader color="#C0EEE4" size={100} />
    </div>
  );
}
