import React from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";

export default function Header({ children }) {
  return (
    <div className="flex px-10 py-5 shadow shadow-blue-300 justify-between items-center">
      <NavLink to="/">
        <img src="/movieLogo.png" alt="" className="h-10 " />

        <span className="text-red-600 font-medium text-xl">
          CyberFlix {children}
        </span>
      </NavLink>
      <UserNav />
    </div>
  );
}
